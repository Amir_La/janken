import styled from 'styled-components';
import theme from 'theme';

interface ButtonProps {
  readonly width: string;
  readonly bg: string;
  readonly border: boolean;
}

const Button = styled.button<ButtonProps>`
  border:${props => props.border ? `3px solid ${props.color ? props.color : theme.textColor}` : 'none'};
  width: ${props => props.width};
  padding: 10px 15px;
  border-radius: 10px;
  font-size: 22px;
  cursor: pointer;
  font-family: inherit;
  font-weight: 700;
  background-color: ${(props) => props.bg ? props.bg : theme.firstColor};
  color: ${(props) => props.color ? props.color : theme.textColor};
`;

export default Button;