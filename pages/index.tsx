import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import styled from 'styled-components';
import Button from 'components/Button';
import theme from 'theme';

const MainContainer = styled.div`
  height: calc(100vh - 100px);
  background-color: ${theme.backgroundColor.dark};
  padding: 50px;
`;

const TitleContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Title = styled.h1`
  font-size: 32px;
`;

const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 50px;
`;

const Home: NextPage = () => {
  return (
    <MainContainer>
      <Head>
        <title>Janken</title>
        <meta name="description" content="WELCOME TO JANKEN POOOOOOON" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400&display=swap" rel="stylesheet" />
      </Head>
      <TitleContainer>
        <Title> Bienvenue sur Janken, prêt à jouer ? </Title>
      </TitleContainer>
      <ButtonContainer>
        <Button bg={theme.backgroundColor.dark} color={theme.firstColor} border={true} width="200px"> Jouer </Button>
      </ButtonContainer>
    </MainContainer>
  )
}

export default Home
