export default {
  firstColor: '#FF008E',
  secondColor: '#FFF338',
  textColor: '#FFFFFF',
  cancelColor: '#FF449F',
  confirmColor: '#00EAD3',
  backgroundColor: {
    light: '',
    dark: '#0C1E7F'
  }
}